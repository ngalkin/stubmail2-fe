export const AppConfig = ($urlRouterProvider, $stateProvider) => {
  'ngInject';
  $stateProvider
    .state('app', {
      url: '',
      template: '<app></app>'
    });
};
