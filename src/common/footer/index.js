import angular from 'angular';
import component from './footer.component';
import './footer.scss';

export default angular
  .module('app.footer', [])
  .directive('footer', component)
  .name;
