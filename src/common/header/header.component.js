import template from './header.jade';

const component = () => {
  return {
    template,
    restrict: 'E',
    replace: false
  };
};

export default component;
