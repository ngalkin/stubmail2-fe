/* global API_URL */

export const User = ($http) => {
  'ngInject';
  const actions = {};

  actions.all = () => {
    return $http({
      method: 'get',
      url: `${API_URL}/users`
    }).then(response => {
      return response.data;
    });
  };

  actions.find = (query) => {
    return $http({
      method: 'get',
      url: `${API_URL}/users${(query ? `?filter[login][like]=${query}` : '')}`
    }).then(response => {
      return response.data;
    });
  };

  actions.update = data => {
    return $http({
      method: 'put',
      url: `${API_URL}/users/${data._id}`,
      data: data
    }).then(response => {
      return response.data;
    });
  };

  actions.detail = id => {
    return $http({
      method: 'get',
      url: `${API_URL}/users/${id}`
    }).then(response => {
      return response.data;
    });
  };

  return actions;
};
