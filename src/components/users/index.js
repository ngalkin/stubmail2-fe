import angular from 'angular';
import 'angular-moment';
import 'angular-xeditable';

import 'x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css';

import {UsersListComponent} from './users.components';
import {User} from './users.services';
import {UsersConfig} from './users.config';
import {UsersRun} from './users.run';

export default angular
  .module('app.users', ['ui.router', 'angularMoment', 'xeditable'])
  .service('User', User)
  .directive('users', UsersListComponent)
  .config(UsersConfig)
  .run(UsersRun)
  .name;
