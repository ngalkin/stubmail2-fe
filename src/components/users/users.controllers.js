export class UsersListController {
  /* @ngInject*/
  constructor(User, $scope, $stateParams, $state, $q) {
    this.$q = $q;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.User = User;
    this.type = $scope.type;
    this.checkAll = false;
    this.list();
  }

  list() {
    this.User.all().then(items => this.items = items);
  }

  update(data) {
    this.User.update(data);
  }
}
