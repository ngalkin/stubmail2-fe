export class ProjectsListController {
  constructor(Project, User, $stateParams, $state) {
    'ngInject';
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.Project = Project;
    this.User = User;
    this.checkAll = false;
    this.list();
  }

  list() {
    this.Project.all().then(items => this.items = items);
  }

  findUsers(query) {
    return this.User.find(query);
  }

  add() {
    this.inserted = {
      title: '',
    };
    this.items.unshift(this.inserted);
  }

  save(data, id) {
    Object.assign(data, {_id: id});
    this.Project.save(data);
  }

  tagAdded(user, project) {
    this.save(project, project._id);
  }

  tagRemoved(user, project) {
    this.save(project, project._id);
  }

  destroy(item) {
    this.Project.destroy(item._id).then(() => {
      this.$state.transitionTo(this.$state.current, this.$stateParams, {
        reload: true,
        inherit: false,
        notify: true
      });
    });
  }
}
