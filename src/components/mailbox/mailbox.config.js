export const MailboxConfig = /* @ngInject*/ ($sceProvider, $urlRouterProvider, $stateProvider) => {
  $sceProvider.enabled(false);
  $urlRouterProvider.otherwise('/mailbox/inbox');
  $stateProvider
    .state('app.mailbox', {
      url: '/mailbox',
      template: '<mailbox></mailbox>'
    })
    .state('app.mailbox.inbox', {
      url: '/inbox/:project',
      params: {
        project: {
          value: null
        }
      },
      template: '<mailbox-list type="inbox"></mailbox-list>'
    })
    .state('app.mailbox.trash', {
      url: '/trash',
      template: '<mailbox-list type="trash"></mailbox-list>'
    })
    .state('app.mailbox.message', {
      url: '/message/:id',
      template: '<mailbox-detail></mailbox-detail>'
    });
};
