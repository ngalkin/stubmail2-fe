export const AuthInterceptor = ($q, $localStorage, $location) => {
  'ngInject';
  return {
    request: config => {
      const token = $localStorage.token;
      if (token) {
        config.headers = config.headers || {};
        // config.headers.Authorization = 'Bearer ' + token;
        config.headers.Authorization = token;
      }
      return config;
    },
    responseError: response => {
      if (!response.status || response.status === 401 || response.status === 403) {
        delete $localStorage.token;
        $location.path('/auth/signin');
      }
      return $q.reject(response);
    }
  };
};
