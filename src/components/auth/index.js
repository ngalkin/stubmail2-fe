import angular from 'angular';
import uiRouter from 'angular-ui-router';
import 'ngstorage/ngStorage';

import {
  LoginComponent,
  LogoutComponent
} from './auth.components';
import {
  AuthConfig
} from './auth.config';
import {
  AuthService
} from './auth.services';

export default angular
  .module('app.auth', [uiRouter, 'ngStorage'])
  .service('AuthService', AuthService)
  .directive('auth', LoginComponent)
  .directive('authLogout', LogoutComponent)
  .config(AuthConfig)
  .name;
